#include <Servo.h>

Servo x; //->180 right
Servo y; //->180 down

const int top_left = A1;
const int top_right = A2;
const int bottom_left = A0;
const int bottom_right = A3;

int top_left_value = 0;
int top_right_value = 0;
int bottom_left_value = 0;
int bottom_right_value = 0;

void setup() {
  // put your setup code here, to run once:

  x.attach(5);
  y.attach(3);

  Serial.begin(9600);

}



void loop() {
  // put your main code here, to run repeatedly:
//
//  Serial.println(x.read());
//  x.write(0);
  top_left_value = analogRead(top_left);
  top_right_value = analogRead(top_right);
  bottom_left_value = analogRead(bottom_left);
  bottom_right_value = analogRead(bottom_right);
  delay(50);
  int avg_bottom = (bottom_left_value + bottom_right_value) / 2; 
  int avg_top = (top_left_value + top_right_value) / 2;
  //if they all have the same average, stop moving.
  if ((avg_top - avg_bottom > 10) or (avg_bottom - avg_top > 10)) {
     if (avg_top > avg_bottom) {
        y.write(y.read() + 1);
      } else {
        y.write(y.read() - 1);
    }
  }
  int avg_left = (bottom_left_value + top_left_value) / 2;
  int avg_right = (bottom_right_value + top_right_value) / 2;
  if ((avg_left - avg_right > 10) or (avg_right - avg_left > 10)) {
     if (avg_left > avg_right) {
        x.write(x.read() + 1);
      } else {
        x.write(x.read() - 1);
    }
  }
}
